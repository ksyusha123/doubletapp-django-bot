import pytest

from app.internal.users.db.models import User


@pytest.mark.smoke
@pytest.mark.django_db
def test_database_smoke():
    User.objects.create(telegram_username="smoke", first_name="smoke")
