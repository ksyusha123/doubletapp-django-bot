import pytest
from django.test import Client


@pytest.mark.smoke
def test_site_smoke():
    resp = Client().get("/admin/")
    assert resp.status_code == 302
