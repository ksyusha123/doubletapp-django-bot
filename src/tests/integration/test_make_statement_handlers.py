import pytest

from app.internal.bot.handlers.transactions.make_statement import enter_end_date, process_dates


@pytest.mark.integration
def test_make_statement_returns_end_if_start_later_end(
    update_mock_small_end_date, context_mock_with_big_user_date_data
):
    assert process_dates(update_mock_small_end_date, context_mock_with_big_user_date_data) == -1


@pytest.mark.integration
def test_process_correct_date_returns_final(update_mock_date, context_mock):
    assert enter_end_date(update_mock_date, context_mock) == 3


@pytest.mark.integration
def test_process_wrong_date_returns_end(update_mock_wrong_date, context_mock):
    assert enter_end_date(update_mock_wrong_date, context_mock) == -1
