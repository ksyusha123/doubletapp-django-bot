from unittest import mock

import pytest

from app.internal.bot.handlers.constants import AddUserToFavourite, Common, RemoveUserFromFavourites
from app.internal.bot.handlers.users.add_favourite_user import final as add_final
from app.internal.bot.handlers.users.delete_favourite_user import final as del_final
from app.internal.users.db.repositories import UserRepository

user_repo = UserRepository()


@pytest.mark.integration
@pytest.mark.django_db
def test_delete_cannot_delete_if_not_in_favs(test_user, test_user_second, update_mock_remove):
    context_mock = mock.MagicMock(user_data={"user": test_user})
    del_final(update_mock_remove, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock_remove.effective_chat.id, text=RemoveUserFromFavourites.NOT_IN_FAVS.value
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_delete(test_user, test_user_second, update_mock_remove):
    user_repo.add_favourite_user(test_user, test_user_second)
    context_mock = mock.MagicMock(user_data={"user": test_user})
    del_final(update_mock_remove, context_mock)
    context_mock.bot.send_message.assert_called_with(
        chat_id=update_mock_remove.effective_chat.id,
        text=Common.SUCCESS.value,
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_add_cannot_add_self(test_user, update_mock_add_self):
    context_mock = mock.MagicMock(user_data={"user": test_user})
    add_final(update_mock_add_self, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock_add_self.effective_chat.id, text=AddUserToFavourite.ADD_SELF_ERROR.value
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_add(test_user, test_user_second, update_mock_add, context_mock):
    context_mock = mock.MagicMock(user_data={"user": test_user})
    add_final(update_mock_add, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock_add.effective_chat.id,
        text=Common.SUCCESS.value,
    )
