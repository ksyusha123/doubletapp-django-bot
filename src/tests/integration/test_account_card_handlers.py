import pytest

from app.internal.bot.handlers.accounts.how_much_on_account import GET_ACCOUNT_AMOUNT, how_much_on_acc
from app.internal.bot.handlers.users.start import start
from tests.utils import create_account


@pytest.mark.integration
@pytest.mark.django_db
def test_how_much_on_acc_returns_end_if_no_user(update_mock_start, context_mock):
    assert how_much_on_acc(update_mock_start, context_mock) == -1


@pytest.mark.integration
@pytest.mark.django_db
def test_how_much_on_acc_returns_end_if_no_accounts(update_mock_start, context_mock):
    start(update_mock_start, context_mock)
    assert how_much_on_acc(update_mock_start, context_mock) == -1


@pytest.mark.integration
@pytest.mark.django_db
def test_how_much_on_acc_returns_next_stage(update_mock_start, context_mock):
    create_account(
        "12345678901234567890", update_mock_start.effective_chat.username, update_mock_start.effective_chat.first_name
    )
    assert how_much_on_acc(update_mock_start, context_mock) == GET_ACCOUNT_AMOUNT
