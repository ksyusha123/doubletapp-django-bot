from datetime import datetime
from unittest import mock

import pytest

from app.internal.users.db.models import User
from tests.conftest import test_user_second


@pytest.fixture
def update_mock_start():
    mocked_effective_chat = mock.MagicMock(
        id="1234567890", username="mocked_username", first_name="mocked", last_name="last"
    )
    mocked_update = mock.MagicMock(effective_chat=mocked_effective_chat)
    return mocked_update


@pytest.fixture
def update_mock_set_phone():
    mocked_msg = mock.MagicMock(text="/set_phone +79641952255")
    mocked_effective_chat = mock.MagicMock(
        id="1234567890", username="mocked_username", first_name="mocked", last_name="last"
    )
    mocked_update = mock.MagicMock(effective_chat=mocked_effective_chat, message=mocked_msg)
    return mocked_update


@pytest.fixture
def update_mock_acc():
    mocked_msg = mock.MagicMock(text="wrong_acc")
    mocked_effective_chat = mock.MagicMock(
        id="1234567890", username="mocked_username", first_name="mocked", last_name="last"
    )
    mocked_update = mock.MagicMock(effective_chat=mocked_effective_chat, message=mocked_msg)
    return mocked_update


@pytest.fixture
def update_mock_negative_amount():
    mocked_msg = mock.MagicMock(text="-100")
    mocked_effective_chat = mock.MagicMock(
        id="1234567890", username="has_acc", first_name="has_acc", last_name="has_acc"
    )
    mocked_update = mock.MagicMock(effective_chat=mocked_effective_chat, message=mocked_msg)
    return mocked_update


@pytest.fixture
def update_mock_too_big_amount():
    mocked_msg = mock.MagicMock(text="1000")
    mocked_effective_chat = mock.MagicMock(
        id="1234567890", username="has_acc", first_name="has_acc", last_name="has_acc"
    )
    mocked_update = mock.MagicMock(effective_chat=mocked_effective_chat, message=mocked_msg)
    return mocked_update


@pytest.fixture
def update_mock_amount():
    mocked_msg = mock.MagicMock(text="10")
    mocked_effective_chat = mock.MagicMock(
        id="1234567890", username="has_acc", first_name="has_acc", last_name="has_acc"
    )
    mocked_update = mock.MagicMock(effective_chat=mocked_effective_chat, message=mocked_msg)
    return mocked_update


@pytest.fixture
def update_mock_add():
    mocked_msg = mock.MagicMock(text="second")
    mocked_effective_chat = mock.MagicMock(id="1234567890", username="first", first_name="first", last_name="first")
    mocked_update = mock.MagicMock(effective_chat=mocked_effective_chat, message=mocked_msg)
    return mocked_update


@pytest.fixture
def update_mock_add_self():
    mocked_msg = mock.MagicMock(text="first")
    mocked_effective_chat = mock.MagicMock(id="1234567890", username="first", first_name="first", last_name="first")
    mocked_update = mock.MagicMock(effective_chat=mocked_effective_chat, message=mocked_msg)
    return mocked_update


@pytest.fixture
def update_mock_remove():
    mocked_msg = mock.MagicMock(text="@second")
    mocked_effective_chat = mock.MagicMock(id="1234567890", username="first", first_name="first", last_name="first")
    mocked_update = mock.MagicMock(effective_chat=mocked_effective_chat, message=mocked_msg)
    return mocked_update


@pytest.fixture
def context_mock():
    context_mock = mock.MagicMock()
    context_mock.bot.send_message = mock.MagicMock()
    return context_mock


@pytest.fixture
def context_mock_with_user_acc_data():
    context_mock = mock.MagicMock(user_data={"from": "98765432100123654789", "to": "98765432100123654780"})
    return context_mock


@pytest.fixture
def context_mock_with_big_user_date_data():
    context_mock = mock.MagicMock(user_data={"start_date": datetime(2023, 10, 10).date()})
    return context_mock


@pytest.fixture
def update_mock_small_end_date():
    mocked_msg = mock.MagicMock(text="30.04.2020")
    mocked_update = mock.MagicMock(message=mocked_msg)
    return mocked_update


@pytest.fixture
def update_mock_date():
    mocked_msg = mock.MagicMock(text="30.04.2020")
    mocked_update = mock.MagicMock(message=mocked_msg)
    return mocked_update


@pytest.fixture
def update_mock_wrong_date():
    mocked_msg = mock.MagicMock(text="50.04.2020")
    mocked_update = mock.MagicMock(message=mocked_msg)
    return mocked_update
