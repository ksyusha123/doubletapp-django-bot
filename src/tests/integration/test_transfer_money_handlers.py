import pytest

from app.internal.accounts.db.models import Account
from app.internal.bot.handlers.users.start import start
from app.internal.bot.handlers.users.transfer_money import (
    FROM_ACC,
    POSTCARD_CHOSE,
    process_account_destination,
    process_username_destination,
    transfer_final,
    transfer_money,
)
from app.internal.transactions.db.models import Transaction
from tests.utils import create_account


@pytest.mark.integration
@pytest.mark.django_db
def test_transfer_money_returns_end_if_no_user(update_mock_start, context_mock):
    assert transfer_money(update_mock_start, context_mock) == -1


@pytest.mark.integration
@pytest.mark.django_db
def test_transfer_money_returns_end_if_no_accounts_from(update_mock_start, context_mock):
    start(update_mock_start, context_mock)
    assert transfer_money(update_mock_start, context_mock) == -1


@pytest.mark.integration
@pytest.mark.django_db
def test_transfer_money_returns_from_acc(update_mock_start, context_mock):
    create_account(
        "12346578903216549874", update_mock_start.effective_chat.username, update_mock_start.effective_chat.first_name
    )
    assert transfer_money(update_mock_start, context_mock) == FROM_ACC


@pytest.mark.integration
@pytest.mark.django_db
def test_process_acc_returns_end_if_no_acc(update_mock_acc, context_mock):
    create_account(
        "12346578903216549874", update_mock_acc.effective_chat.username, update_mock_acc.effective_chat.first_name
    )
    assert process_account_destination(update_mock_acc, context_mock) == -1


@pytest.mark.integration
@pytest.mark.django_db
def test_process_username_returns_end_if_no_acc(update_mock_acc, context_mock):
    create_account(
        "12346578903216549874", update_mock_acc.effective_chat.username, update_mock_acc.effective_chat.first_name
    )
    assert process_username_destination(update_mock_acc, context_mock) == -1


@pytest.mark.integration
@pytest.mark.django_db
def test_final_returns_end_if_negative_amount(
    update_mock_negative_amount, context_mock_with_user_acc_data, test_account
):
    assert transfer_final(update_mock_negative_amount, context_mock_with_user_acc_data) == -1
    assert Account.objects.get(number=test_account.number).amount == 100


@pytest.mark.integration
@pytest.mark.django_db
def test_final_returns_end_if_amount_bigger_than_on_acc(
    update_mock_too_big_amount, context_mock_with_user_acc_data, test_account
):
    assert transfer_final(update_mock_too_big_amount, context_mock_with_user_acc_data) == -1
    assert Account.objects.get(number=test_account.number).amount == 100


@pytest.mark.integration
@pytest.mark.django_db
def test_transfer_money_final(update_mock_amount, context_mock_with_user_acc_data, test_account, test_account_second):
    assert transfer_final(update_mock_amount, context_mock_with_user_acc_data) == POSTCARD_CHOSE
    assert Account.objects.get(number=test_account.number).amount == 90
    assert Account.objects.get(number=test_account_second.number).amount == 110


@pytest.mark.integration
@pytest.mark.django_db
def test_transfer_money_log_operation(
    update_mock_amount, context_mock_with_user_acc_data, test_account, test_account_second
):
    transfer_final(update_mock_amount, context_mock_with_user_acc_data)
    assert Transaction.objects.filter(account_id=test_account.number).exists()
    assert Transaction.objects.filter(account_id=test_account_second.number).exists()
