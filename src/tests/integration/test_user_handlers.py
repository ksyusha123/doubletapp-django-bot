import pytest

from app.internal.bot.handlers.constants import MeHints, StartHints
from app.internal.bot.handlers.users.me import me
from app.internal.bot.handlers.users.start import start
from app.internal.users.db.models import User


@pytest.mark.integration
@pytest.mark.django_db
def test_start_handler(update_mock_start, context_mock):
    start(update_mock_start, context_mock)
    assert User.objects.get(telegram_username=update_mock_start.effective_chat.username)
    assert len(context_mock.bot.send_message.call_args_list) == 2


@pytest.mark.integration
@pytest.mark.django_db
def test_start_handler_when_already_user(update_mock_start, context_mock):
    start(update_mock_start, context_mock)
    start(update_mock_start, context_mock)
    context_mock.bot.send_message.assert_called_with(
        chat_id=update_mock_start.effective_chat.id, text=StartHints.ALREADY_USER.value
    )


# @pytest.mark.integration
# @pytest.mark.django_db
# def test_set_phone(update_mock_set_phone, context_mock):
#     start(update_mock_set_phone, context_mock)
#     set_phone(update_mock_set_phone, context_mock)
#     assert User.objects.get(telegram_username=update_mock_set_phone.effective_chat.username).phone_number
#     context_mock.bot.send_message.assert_called_with(
#         chat_id=update_mock_set_phone.effective_chat.id, text=SetPhoneHints.SUCCESS.value
#     )
#
#
# @pytest.mark.integration
# @pytest.mark.django_db
# def test_me(update_mock_set_phone, context_mock):
#     start(update_mock_set_phone, context_mock)
#     set_phone(update_mock_set_phone, context_mock)
#     me(update_mock_set_phone, context_mock)
#     context_mock.bot.send_message.assert_called()


@pytest.mark.integration
@pytest.mark.django_db
def test_me_when_no_phone_number(update_mock_start, context_mock):
    start(update_mock_start, context_mock)
    me(update_mock_start, context_mock)
    context_mock.bot.send_message.assert_called_with(
        chat_id=update_mock_start.effective_chat.id, text=MeHints.NO_PHONE_NUMBER.value
    )


@pytest.mark.integration
@pytest.mark.django_db
def test_me_when_no_user(update_mock_start, context_mock):
    me(update_mock_start, context_mock)
    context_mock.bot.send_message.assert_called_once_with(
        chat_id=update_mock_start.effective_chat.id, text=MeHints.NO_USER.value
    )
