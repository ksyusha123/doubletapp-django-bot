import pytest

from app.internal.accounts.db.models import Account
from app.internal.users.db.models import User


@pytest.mark.django_db
def create_account(number, telegram_username, first_name):
    user = User.objects.create(telegram_username=telegram_username, first_name=first_name)
    acc = Account.objects.create(owner=user, amount=100, number=number)
    return acc
