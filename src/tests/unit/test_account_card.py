import pytest

from app.internal.accounts.db.repositories import AccountsRepository

accounts_repo = AccountsRepository()


@pytest.mark.unit
@pytest.mark.django_db
def test_transfer_money(test_account, test_account_second):
    accounts_repo.transfer_money(test_account, test_account_second, 20)
    assert test_account.amount == 80
    assert test_account_second.amount == 120
