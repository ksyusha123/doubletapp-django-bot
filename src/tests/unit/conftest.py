from datetime import datetime

import jwt
import pytest

from app.internal.authentication.db.repositories import generate_jwt_token
from config.settings import ACCESS_TOKEN_EXPIRES_PERIOD


@pytest.fixture
def test_access_token(username="first"):
    return generate_jwt_token(username, ACCESS_TOKEN_EXPIRES_PERIOD)


@pytest.fixture
def test_incorrect_token(username="first"):
    return jwt.encode(
        {"username": username, "exp": datetime.now().timestamp() + ACCESS_TOKEN_EXPIRES_PERIOD},
        "WRONG_SECRET",
        algorithm="HS256",
    )
