import pytest
from django.core.exceptions import ValidationError

from app.internal.users.db.repositories import validate_phone_number


@pytest.mark.unit
@pytest.mark.parametrize("phone_number", ["+79641952245", "89641952245", "+7-964-195-22-45", "8-964-195-22-45"])
def test_validate_correct_phone_number(phone_number):
    assert validate_phone_number(phone_number) is None


@pytest.mark.unit
@pytest.mark.parametrize("phone_number", ["+796419522456789", "+796415", "99999999999", "123456789"])
def test_validate_incorrect_phone_number(phone_number):
    with pytest.raises(ValidationError):
        validate_phone_number(phone_number)
