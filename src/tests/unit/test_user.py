import json

import pytest

from app.internal.users.db.repositories import UserRepository

user_repo = UserRepository()


@pytest.mark.unit
@pytest.mark.django_db
def test_me_endpoint_when_no_phone_number(test_user, test_client, test_access_token):
    auth_headers = {
        "HTTP_AUTHORIZATION": "Bearer " + test_access_token,
    }
    response = test_client.get("/api/users/me", **auth_headers)
    assert response.status_code == 400
    response_json = json.loads(response.content)
    assert response_json["message"] == "phone number must be set"


@pytest.mark.unit
@pytest.mark.django_db
def test_me_endpoint_when_incorrect_token(test_client, test_incorrect_token):
    auth_headers = {
        "HTTP_AUTHORIZATION": "Bearer " + test_incorrect_token,
    }
    response = test_client.get("/api/users/me", **auth_headers)
    assert response.status_code == 401


@pytest.mark.unit
@pytest.mark.django_db
def test_me_endpoint(test_user, test_client, test_access_token):
    user_repo.set_phone(test_user, "+79645879922")
    auth_headers = {
        "HTTP_AUTHORIZATION": "Bearer " + test_access_token,
    }
    response = test_client.get("/api/users/me", **auth_headers)
    response_json = json.loads(response.content)
    assert response.status_code == 200
    assert response_json["telegram_username"] == test_user.telegram_username
    assert response_json["id"] == str(test_user.id)
    assert response_json["first_name"] == test_user.first_name
    assert response_json["last_name"] == test_user.last_name


@pytest.mark.unit
@pytest.mark.django_db
def test_add_user_to_favourites(test_user, test_user_second):
    user_repo.add_favourite_user(test_user, test_user_second)
    assert test_user_second in test_user.favourite_users.all()
    assert test_user not in test_user_second.favourite_users.all()


@pytest.mark.unit
@pytest.mark.django_db
def test_remove_user_from_favourites(test_user, test_user_second):
    user_repo.add_favourite_user(test_user_second, test_user)
    user_repo.delete_favourite_user(test_user, test_user_second)
    assert not test_user.favourite_users.all()
    assert test_user in test_user_second.favourite_users.all()
