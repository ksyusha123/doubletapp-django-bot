import pytest
from django.test import Client
from utils import create_account

from app.internal.users.db.models import User


@pytest.fixture
@pytest.mark.django_db
def test_user(telegram_username="first", first_name="first"):
    return User.objects.create(telegram_username=telegram_username, first_name=first_name)


@pytest.fixture
@pytest.mark.django_db
def test_user_second(telegram_username="second", first_name="second"):
    return User.objects.create(telegram_username=telegram_username, first_name=first_name)


@pytest.fixture
def test_client():
    return Client()


@pytest.fixture
def test_account():
    return create_account("98765432100123654789", "has_acc", "has_acc")


@pytest.fixture
def test_account_second():
    return create_account("98765432100123654780", "has_acc_too", "has_acc_too")
