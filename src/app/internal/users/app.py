from ninja import NinjaAPI

from .db.repositories import UserRepository
from .metrics import config_users_metrics
from .presentation.handlers import UserHandlers
from .presentation.routers import add_users_router


def add_users_api(api: NinjaAPI):
    user_repo = UserRepository()
    user_handler = UserHandlers(user_repo=user_repo)
    config_users_metrics(user_repo)
    add_users_router(api, user_handler)
