from typing import List

from ninja import NinjaAPI, Router

from app.internal.common.responses import ErrorResponse, SuccessResponse
from app.internal.users.presentation.handlers import UserHandlers

from .entities import UserOut


def add_users_router(api: NinjaAPI, user_handlers: UserHandlers):
    users_router = get_users_router(user_handlers)
    api.add_router("/users", users_router)


def get_users_router(user_handlers: UserHandlers):
    router = Router(tags=["users"])

    router.add_api_operation(
        "",
        ["POST"],
        user_handlers.add_user,
        auth=None,
        response={201: UserOut},
    )

    router.add_api_operation(
        "/me",
        ["GET"],
        user_handlers.get_me,
        response={200: UserOut, 400: ErrorResponse},
    )

    router.add_api_operation(
        "/set_phone",
        ["PATCH"],
        user_handlers.set_phone,
        response={200: SuccessResponse, 400: ErrorResponse},
    )

    router.add_api_operation(
        "/favourite_users",
        ["GET"],
        user_handlers.get_favourite_users,
        response={200: List[str]},
    )

    router.add_api_operation(
        "/favourite_users",
        ["POST"],
        user_handlers.add_favourite_user,
        response={200: SuccessResponse, 400: ErrorResponse},
    )

    router.add_api_operation(
        "/favourite_users",
        ["DELETE"],
        user_handlers.delete_favourite_user,
        response={200: SuccessResponse, 400: ErrorResponse},
    )

    return router
