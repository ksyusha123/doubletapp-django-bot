from uuid import UUID

from ninja.orm import create_schema

from ..db.models import User

UserSchema = create_schema(User, exclude=["id", "password", "favourite_users"])


class UserOut(UserSchema):
    id: UUID


class UserIn(UserSchema):
    password: str
