from typing import List

from django.db.utils import IntegrityError
from ninja import Body

from app.internal.common.exceptions import AlreadyUserException, BadRequestException, NotFoundException
from app.internal.common.responses import SuccessResponse

from ..db.repositories import UserRepository
from .entities import UserIn, UserOut


class UserHandlers:
    def __init__(self, user_repo: UserRepository):
        self._user_repo = user_repo

    def add_user(self, request, user_data: UserIn = Body(...)) -> UserOut:
        try:
            user = self._user_repo.add_user(user_data)
            return user
        except IntegrityError:
            raise AlreadyUserException(user_data.telegram_username)

    def get_me(self, request) -> UserOut:
        return self._user_repo.get_me(request.user)

    def set_phone(self, request, phone_number: str) -> SuccessResponse:
        self._user_repo.set_phone(request.user, phone_number)
        return SuccessResponse(success=True)

    def get_favourite_users(self, request) -> List:
        return list(self._user_repo.get_favourite_users(request.user))

    def add_favourite_user(self, request, username: str) -> SuccessResponse:
        user_to_add = self._user_repo.get_user_by_username(username)
        if not user_to_add:
            raise NotFoundException(username, "user")
        self._user_repo.add_favourite_user(request.user, user_to_add)
        return SuccessResponse(success=True)

    def delete_favourite_user(self, request, username: str) -> SuccessResponse:
        user = request.user
        favourite_users = set(self._user_repo.get_favourite_users(user))
        if username not in favourite_users:
            raise BadRequestException(f"{username} is not in your favourites")
        user_to_remove = self._user_repo.get_user_by_username(username)
        self._user_repo.delete_favourite_user(user, user_to_remove)
        return SuccessResponse(success=True)
