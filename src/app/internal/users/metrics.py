from prometheus_client import Gauge

from .db.repositories import UserRepository


def config_users_metrics(user_repo: UserRepository):
    users_count = Gauge("users_count", "bank users count")
    users_count.set_function(user_repo.get_users_count)
