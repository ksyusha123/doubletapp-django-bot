from django.core.exceptions import ValidationError
from django.db.models.query import QuerySet

from app.internal.common.exceptions import BadRequestException
from app.internal.common.password_service import hash_password
from app.internal.users.db.models import User, validate_phone_number

from ..presentation.entities import UserIn, UserOut


class UserRepository:
    def add_user(self, user_data: UserIn) -> UserOut:
        validate_phone_number(user_data.phone_number)
        hashed_password = hash_password(user_data.password)
        user = User.objects.create(
            telegram_username=user_data.telegram_username,
            first_name=user_data.first_name,
            last_name=user_data.last_name,
            phone_number=user_data.phone_number,
            password=hashed_password,
        )
        return UserOut.from_orm(user)

    def get_me(self, user: User) -> UserOut:
        if not user.phone_number:
            raise BadRequestException("phone number must be set")
        return UserOut.from_orm(user)

    def set_phone(self, user: User, phone_number: str):
        validate_phone_number(phone_number)
        user.phone_number = phone_number
        user.save()

    def get_favourite_users(self, user: User) -> QuerySet[str]:
        return user.favourite_users.values_list("telegram_username", flat=True)

    def add_favourite_user(self, user: User, user_to_add: User):
        user.favourite_users.add(user_to_add)

    def get_user_by_username(self, username: str) -> User | None:
        return User.objects.filter(telegram_username=username).first()

    def delete_favourite_user(self, user: User, user_to_remove: User):
        user.favourite_users.remove(user_to_remove)

    def validate_by_username(self, user: User, password: str):
        hashed_password = hash_password(password)
        if user.password != hashed_password:
            raise ValidationError(f"Wrong password for user {user.telegram_username}")

    def check_user_exists(self, username: str) -> bool:
        return User.objects.filter(telegram_username=username).exists()

    def get_users_count(self) -> int:
        return User.objects.count()
