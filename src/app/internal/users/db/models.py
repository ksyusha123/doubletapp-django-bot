from uuid import uuid4

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django_prometheus.models import ExportModelOperationsMixin


def validate_phone_number(phone_number: str):
    if not settings.PHONE_NUMBER_REGEX.match(phone_number):
        raise ValidationError("Incorrect phone number")


class User(ExportModelOperationsMixin("user"), models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    telegram_username = models.CharField(max_length=255, unique=True, null=False)
    password = models.CharField(max_length=255, null=False, editable=False)
    first_name = models.CharField(max_length=255, null=True, blank=True)
    last_name = models.CharField(max_length=255, null=True, blank=True)
    phone_number = models.CharField(validators=[validate_phone_number], max_length=17, blank=True)
    favourite_users = models.ManyToManyField("self", blank=True, symmetrical=False)

    def __str__(self):
        return self.telegram_username
