from django.contrib import admin

from ..db.models import Account


@admin.register(Account)
class SimpleUserAdmin(admin.ModelAdmin):
    pass
