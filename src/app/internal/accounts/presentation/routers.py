from ninja import NinjaAPI, Router

from app.internal.accounts.presentation.handlers import AccountHandlers
from app.internal.common.responses import ErrorResponse

from .entities import AccountOut


def add_accounts_router(api: NinjaAPI, account_handlers: AccountHandlers):
    accounts_router = get_accounts_router(account_handlers)
    api.add_router("/accounts", accounts_router)


def get_accounts_router(accounts_handlers: AccountHandlers):
    router = Router(tags=["accounts"])

    router.add_api_operation(
        "/{int:account_number}",
        ["GET"],
        accounts_handlers.get_account_by_number,
        response={200: AccountOut, 403: ErrorResponse},
    )

    return router
