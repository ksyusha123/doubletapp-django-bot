from ninja.orm import create_schema

from ..db.models import Account

AccountSchema = create_schema(Account, exclude=["number", "owner"])


class AccountOut(AccountSchema):
    ...
