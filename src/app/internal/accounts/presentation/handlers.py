from ninja import Path

from app.internal.common.exceptions import NotFoundException, PermissionDenied

from ..db.repositories import AccountsRepository
from .entities import AccountOut


class AccountHandlers:
    def __init__(self, account_repo: AccountsRepository):
        self._account_repo = account_repo

    def get_account_by_number(self, request, account_number: str = Path(...)) -> AccountOut:
        account = self._account_repo.get_account_by_number(account_number)
        if not account:
            raise NotFoundException(account_number, "account")
        if account.owner != request.user:
            raise PermissionDenied(account_number, "account")
        return AccountOut.from_orm(account)
