from prometheus_client import Gauge

from .db.repositories import AccountsRepository


def config_accounts_metrics(accounts_repo: AccountsRepository):
    bank_money_amount = Gauge("bank_money_amount", "bank money amount")
    bank_money_amount.set_function(accounts_repo.get_bank_money_amount)
