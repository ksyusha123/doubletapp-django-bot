import re

from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.db import models

from app.internal.users.db.models import User


def validate_account_number(account_number: str):
    if not re.match(r"^\d{20}$", account_number):
        raise ValidationError("Incorrect account number")


class Account(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=10, decimal_places=2, default=0, validators=[MinValueValidator(0)])
    number = models.CharField(primary_key=True, max_length=20, validators=[validate_account_number])

    def __str__(self):
        return f"owner: {self.owner.telegram_username}\namount: {self.amount}\nnumber: {self.number}\n"
