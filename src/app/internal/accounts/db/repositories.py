import logging
from decimal import Decimal

from django.db import transaction
from django.db.models.query import QuerySet
from prometheus_client import Summary

from app.internal.users.db.models import User
from app.internal.users.db.repositories import UserRepository

from .models import Account

logger = logging.getLogger(__name__)
user_repo = UserRepository()
transfer_money_time = Summary("transfer_money_time", "transfer money time")


class AccountsRepository:
    def get_user_accounts_number(self, user: User) -> QuerySet[str]:
        return Account.objects.filter(owner_id=user.id).values_list("number", flat=True)

    def get_amount_for_account_by_number(self, account_number: str) -> Decimal:
        account = Account.objects.get(pk=account_number)
        return account.amount

    def get_account_by_number(self, account_number: str) -> Account | None:
        return Account.objects.filter(pk=account_number).first()

    @transfer_money_time.time()
    def transfer_money(self, account_from: Account, account_to: Account, amount):
        try:
            with transaction.atomic():
                account_from.amount -= amount
                account_to.amount += amount
                account_from.save()
                account_to.save()
        except transaction.TransactionManagementError:
            logger.error(
                f"Fail transfer {amount} from {account_from.number} to {account_to.number} due to transaction error"
            )
            raise

    def get_bank_money_amount(self) -> float:
        return float(sum(Account.objects.all().values_list("amount", flat=True)))
