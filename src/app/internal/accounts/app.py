from ninja import NinjaAPI

from .db.repositories import AccountsRepository
from .metrics import config_accounts_metrics
from .presentation.handlers import AccountHandlers
from .presentation.routers import add_accounts_router


def add_accounts_api(api: NinjaAPI):
    account_repo = AccountsRepository()
    account_handlers = AccountHandlers(account_repo=account_repo)
    config_accounts_metrics(account_repo)
    add_accounts_router(api, account_handlers)
