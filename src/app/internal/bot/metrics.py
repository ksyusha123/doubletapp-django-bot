from prometheus_client import Counter

transaction_tries_by_username = Counter("transaction_tries_by_username", "transaction tries count by username")
transaction_tries_by_card = Counter("transaction_tries_by_card", "transaction tries count by card")
transaction_tries_by_account = Counter("transaction_tries_by_account", "transaction tries count by account")
