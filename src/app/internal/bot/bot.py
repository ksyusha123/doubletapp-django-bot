from telegram import Bot, Update
from telegram.ext import Dispatcher

from config.settings import BOT_TOKEN

from .handlers.accounts.how_much_on_account import how_much_on_acc_conv_handler
from .handlers.cards.how_much_on_card import how_much_on_card_conv_handler
from .handlers.transactions.find_people_interacted_with import find_people_interacted_with_command_handler
from .handlers.transactions.make_statement import make_statement_conv_handler
from .handlers.transactions.new_transactions import new_transactions_command_handler
from .handlers.users.add_favourite_user import add_favourite_user_conv_handler
from .handlers.users.delete_favourite_user import delete_favourite_user_conv_handler
from .handlers.users.favourite_users import favourite_users_command_handler
from .handlers.users.me import me_command_handler
from .handlers.users.set_phone import set_phone_conv_handler
from .handlers.users.start import start_command_handler
from .handlers.users.transfer_money import transfer_money_conv_handler


class BankBot:
    def __init__(self):
        self.bot = Bot(BOT_TOKEN)
        self.dispatcher = Dispatcher(self.bot, update_queue=None, workers=1, use_context=True)
        self.register_handlers()

    def process_update(self, update_json):
        update = Update.de_json(update_json, self.bot)
        self.dispatcher.process_update(update)

    def register_handlers(self):
        self.dispatcher.add_handler(start_command_handler)
        self.dispatcher.add_handler(set_phone_conv_handler)
        self.dispatcher.add_handler(me_command_handler)
        self.dispatcher.add_handler(add_favourite_user_conv_handler)
        self.dispatcher.add_handler(delete_favourite_user_conv_handler)
        self.dispatcher.add_handler(favourite_users_command_handler)
        self.dispatcher.add_handler(how_much_on_acc_conv_handler)
        self.dispatcher.add_handler(how_much_on_card_conv_handler)
        self.dispatcher.add_handler(transfer_money_conv_handler)
        self.dispatcher.add_handler(make_statement_conv_handler)
        self.dispatcher.add_handler(find_people_interacted_with_command_handler)
        self.dispatcher.add_handler(new_transactions_command_handler)


bot = BankBot()
