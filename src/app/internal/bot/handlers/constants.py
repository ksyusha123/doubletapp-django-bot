from enum import Enum


class SetPhoneHints(Enum):
    ENTER_PHONE_NUMBER = "Enter phone number"
    SUCCESS = "Saved your phone number to the database"
    NO_USER = "I don't know you :(\nPlease use /start"


class StartHints(Enum):
    ALREADY_USER = "You are already our user"
    SUCCESS = "Successfully registered you!"
    PASSWORD = "Your password:"


class MeHints(Enum):
    NO_USER = "I don't know you :(\n" "Please use /start and /set_phone"
    NO_PHONE_NUMBER = "I don't know your phone number :(\nPlease use /set_phone"
    SUCCESS = "Information about you:"


class HowMuchOnAccountHints(Enum):
    NO_USER = "I don't know you :(\n" "Please use /start and /set_phone"
    NO_ACCOUNTS = "You don't have any accounts yet"
    SUCCESS = "Choose item"


class HowMuchOnCardHints(Enum):
    NO_USER = "I don't know you :(\n" "Please use /start and /set_phone"
    NO_CARDS = "You don't have any cards yet"
    SUCCESS = "Choose item"


class TransferMoneyHints(Enum):
    ENTER_DESTINATION = "Enter destination (may be account/card number, telegram username)"
    ENTER_AMOUNT = "Enter amount"
    CHOOSE_ACCOUNT = "Choose account to transfer money from"
    WRONG_AMOUNT = "Amount has to be positive"
    TRANSACTION_ERROR = "Oops.. Something went wrong :( Try again"
    SUCCESS = "Super! Transfer is successful\nWould you like to attach a postcard?"
    WRONG_CARD_NUMBER = "You don't have card with given number"
    CHOOSE_DESTINATION_ACCOUNT = "Choose account you want to transfer money"
    NO_ACCOUNTS_TO = "User does not have any accounts yet"
    NOT_ENOUGH = "You don't have enough money"
    WRONG_DESTINATION = "Wrong destination"
    SEND_POSTCARD = "Send a postcard. Don't compress image"
    ATTACHED_POSTCARD = "Attached your postcard to transaction"
    FINAL = "Ok! Good luck!"


class ShowFavouriteUsers(Enum):
    NO_FAV_USERS = "You have no favourite users. Add with /add_user_to_favourites"


class AddUserToFavourite(Enum):
    WRONG_ARGUMENTS = (
        "Use this command with user you want to add\nExample: /add_user_to_favourites @my_friend ("
        "/remove_user_from_favourites @my_friend)"
    )
    WRONG_USER_TO_ADD = "User you want to add is not our client"
    SUCCESS = "Success"
    ADD_SELF_ERROR = "You can't add yourself to favourites"
    ENTER_USERNAME = "Enter username"


class RemoveUserFromFavourites(Enum):
    SUCCESS = "Successfully remove "
    NOT_IN_FAVS = "This user isn't your favourite"


class MakeAccountStatement(Enum):
    NO_USER = "I don't know you :(\nPlease use /start"
    NO_ACCOUNTS = "You don't have any accounts yet"
    ENTER_START_DATE = "Enter start date in format dd.mm.yyyy"
    ENTER_END_DATE = "Enter end date in the same format"
    WRONG_DATE = "Wrong date"
    END_HAS_TO_BE_LATER = "End date has to be bigger than start date"
    BY = "Choose item which by you want to make a statement"


class FindPeopleInteractedWith(Enum):
    NO_PEOPLE = "You have never interacted with anybody. Transfer money to " "someone"


class Common(Enum):
    NO_USER = "I don't know you :(\nPlease use /start"
    NO_ACCOUNTS = "You don't have any accounts yet"
    NO_CARDS = "You don't have any cards yet"
    CHOOSE_ITEM = "Choose item"
    ENTER_USERNAME = "Enter username"
    SUCCESS = "Success"


class NewTransactions(Enum):
    NO_NEW_TRANSACTIONS = "You have no new transactions"
