from telegram import Update
from telegram.ext import CallbackContext, CommandHandler

from app.internal.bot.handlers import user_repo
from app.internal.bot.handlers.constants import MeHints


def me(update: Update, context: CallbackContext):
    user = user_repo.get_user_by_username(update.effective_chat.username)
    if not user:
        message = MeHints.NO_USER.value
    elif not user.phone_number:
        message = MeHints.NO_PHONE_NUMBER.value
    else:
        last_name = "" if not user.last_name else user.last_name
        str_tokens = [
            MeHints.SUCCESS.value,
            user.telegram_username,
            user.first_name + last_name,
            user.phone_number,
            str(user.id),
        ]
        message = "\n".join(str_tokens)
    context.bot.send_message(chat_id=update.effective_chat.id, text=message)


me_command_handler = CommandHandler("me", me)
