import logging
from decimal import Decimal

from django.db.utils import IntegrityError
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler, Filters, MessageHandler

from app.internal.bot.handlers import accounts_repo, card_repo, postcard_repo, transactions_repo, user_repo
from app.internal.bot.metrics import (
    transaction_tries_by_account,
    transaction_tries_by_card,
    transaction_tries_by_username,
)

from ..constants import Common, TransferMoneyHints
from ..utils import cancel, choose_account, make_keyboard

logger = logging.getLogger(__name__)

FROM_ACC, DESTINATION, SPECIFY_DESTINATION, TRANSFER_FINAL, POSTCARD_CHOSE, POSTCARD = range(6)


def transfer_money(update: Update, context: CallbackContext) -> int:
    return choose_account(update, context, Common.NO_ACCOUNTS.value, FROM_ACC)


def from_acc(update: Update, context: CallbackContext) -> int:
    account_number = update.message.text
    context.user_data["from"] = account_number
    update.message.reply_text(TransferMoneyHints.ENTER_DESTINATION.value, reply_markup=ReplyKeyboardRemove())
    return DESTINATION


def process_account_destination(update: Update, context: CallbackContext) -> int:
    account_number = update.message.text
    account = accounts_repo.get_account_by_number(account_number)
    if not account:
        context.bot.send_message(chat_id=update.effective_chat.id, text=TransferMoneyHints.WRONG_DESTINATION.value)
        return ConversationHandler.END
    context.user_data["to"] = account_number
    transaction_tries_by_account.inc()
    return enter_amount(update, context)


def process_card_number_destination(update: Update, context: CallbackContext) -> int:
    account = card_repo.get_account_by_card_number(update.message.text)
    if not account:
        context.bot.send_message(chat_id=update.effective_chat.id, text=TransferMoneyHints.WRONG_DESTINATION.value)
        return ConversationHandler.END
    context.user_data["to"] = account.number
    transaction_tries_by_card.inc()
    return enter_amount(update, context)


def process_username_destination(update: Update, context: CallbackContext) -> int:
    username = update.message.text[1:]
    user = user_repo.get_user_by_username(username)
    if not user:
        context.bot.send_message(chat_id=update.effective_chat.id, text=TransferMoneyHints.WRONG_DESTINATION.value)
        return ConversationHandler.END

    transaction_tries_by_username.inc()

    accounts_numbers = accounts_repo.get_user_accounts_number(user)
    if not accounts_numbers:
        context.bot.send_message(chat_id=update.effective_chat.id, text=TransferMoneyHints.NO_ACCOUNTS_TO.value)
        return ConversationHandler.END

    keyboard = [list(accounts_numbers)]
    reply_markup = ReplyKeyboardMarkup(keyboard, one_time_keyboard=True, resize_keyboard=True)
    update.message.reply_text(TransferMoneyHints.CHOOSE_DESTINATION_ACCOUNT.value, reply_markup=reply_markup)
    return SPECIFY_DESTINATION


def specify_destination(update: Update, context: CallbackContext) -> int:
    account_number = update.message.text
    account = accounts_repo.get_account_by_number(account_number)
    if not account:
        context.bot.send_message(chat_id=update.effective_chat.id, text=TransferMoneyHints.WRONG_DESTINATION.value)
        return ConversationHandler.END
    context.user_data["to"] = account_number
    update.message.reply_text(reply_markup=ReplyKeyboardRemove(), text=TransferMoneyHints.ENTER_AMOUNT.value)
    return TRANSFER_FINAL


def enter_amount(update: Update, _) -> int:
    update.message.reply_text(TransferMoneyHints.ENTER_AMOUNT.value)
    return TRANSFER_FINAL


def transfer_final(update: Update, context: CallbackContext) -> int:
    amount = Decimal(update.message.text)
    if amount <= 0:
        update.message.reply_text(TransferMoneyHints.WRONG_AMOUNT.value)
        return ConversationHandler.END
    acc_from = accounts_repo.get_account_by_number(context.user_data.get("from"))
    acc_to = accounts_repo.get_account_by_number(context.user_data.get("to"))
    if amount > acc_from.amount:
        update.message.reply_text(TransferMoneyHints.NOT_ENOUGH.value)
        return ConversationHandler.END
    try:
        accounts_repo.transfer_money(acc_from, acc_to, amount)
        first_transaction, second_transaction = transactions_repo.create_transaction(
            acc_from, acc_to, -amount, acc_from.amount
        ), transactions_repo.create_transaction(acc_to, acc_from, amount, acc_to.amount)
        logger.info(f"Success transfer {first_transaction.id}: " f" {acc_from.number} ->" f" {acc_to.number} {amount}")
    except IntegrityError:
        update.message.reply_text(TransferMoneyHints.TRANSACTION_ERROR.value)
        return ConversationHandler.END
    keyboard = make_keyboard(["Yes", "No"])
    update.message.reply_text(TransferMoneyHints.SUCCESS.value, reply_markup=keyboard)
    context.user_data["first_transaction"] = first_transaction.id
    context.user_data["second_transaction"] = second_transaction.id
    return POSTCARD_CHOSE


def send_postcard_chose(update: Update, context: CallbackContext) -> int:
    answer = update.message.text
    if answer == "No":
        update.message.reply_text(TransferMoneyHints.FINAL.value, reply_markup=ReplyKeyboardRemove())
        return ConversationHandler.END
    update.message.reply_text(TransferMoneyHints.SEND_POSTCARD.value, reply_markup=ReplyKeyboardRemove())
    return POSTCARD


def send_postcard(update: Update, context: CallbackContext) -> int:
    file = update.message.document
    obj = context.bot.get_file(file.file_id)
    content = obj.download_as_bytearray()
    postcard = postcard_repo.add_postcard(content)
    first_transaction = transactions_repo.get_transaction_by_id(context.user_data["first_transaction"])
    second_transaction = transactions_repo.get_transaction_by_id(context.user_data["second_transaction"])
    transactions_repo.attach_postcard(first_transaction, postcard)
    transactions_repo.attach_postcard(second_transaction, postcard)
    update.message.reply_text(TransferMoneyHints.ATTACHED_POSTCARD.value)
    return ConversationHandler.END


transfer_money_conv_handler = ConversationHandler(
    entry_points=[CommandHandler("transfer_money", transfer_money)],
    states={
        FROM_ACC: [MessageHandler(Filters.regex(r"^\d{20}$"), from_acc)],
        DESTINATION: [
            MessageHandler(Filters.regex(r"^\d{16}$"), process_card_number_destination),
            MessageHandler(Filters.regex(r"^\d{20}$"), process_account_destination),
            MessageHandler(Filters.regex(r"^@.*"), process_username_destination),
        ],
        SPECIFY_DESTINATION: [MessageHandler(Filters.regex(r"^\d{20}$"), specify_destination)],
        TRANSFER_FINAL: [
            MessageHandler(Filters.regex(r"^\d+$"), transfer_final),
        ],
        POSTCARD_CHOSE: [MessageHandler(Filters.text, send_postcard_chose)],
        POSTCARD: [MessageHandler(Filters.document.image, send_postcard)],
    },
    fallbacks=[CommandHandler("cancel", cancel)],
)
