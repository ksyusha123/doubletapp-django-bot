from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler, Filters, MessageHandler

from app.internal.bot.handlers import user_repo

from ..constants import AddUserToFavourite, Common
from ..utils import cancel

ENTER_USERNAME = 0


def add_favourite_user(update: Update, context: CallbackContext):
    user = user_repo.get_user_by_username(update.effective_chat.username)
    chat_id = update.effective_chat.id
    if not user:
        context.bot.send_message(chat_id=chat_id, text=Common.NO_USER.value)
        return ConversationHandler.END

    context.user_data["user"] = user
    context.bot.send_message(chat_id=chat_id, text=Common.ENTER_USERNAME.value)
    return ENTER_USERNAME


def final(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    username_to_add = update.message.text.replace("@", "")
    if username_to_add == update.effective_chat.username:
        context.bot.send_message(chat_id=chat_id, text=AddUserToFavourite.ADD_SELF_ERROR.value)
        return ConversationHandler.END
    user_to_add = user_repo.get_user_by_username(username_to_add)
    if not user_to_add:
        context.bot.send_message(chat_id=chat_id, text=AddUserToFavourite.WRONG_USER_TO_ADD.value)
        return ConversationHandler.END
    user_repo.add_favourite_user(context.user_data["user"], user_to_add)
    context.bot.send_message(chat_id=chat_id, text=Common.SUCCESS.value)
    return ConversationHandler.END


add_favourite_user_conv_handler = ConversationHandler(
    entry_points=[CommandHandler("add_favourite_user", add_favourite_user)],
    states={
        ENTER_USERNAME: [MessageHandler(Filters.text, final)],
    },
    fallbacks=[CommandHandler("cancel", cancel)],
)
