from django.db.utils import IntegrityError
from telegram import Update
from telegram.ext import CallbackContext, CommandHandler

from app.internal.bot.handlers import user_repo
from app.internal.bot.handlers.constants import StartHints
from app.internal.common.password_service import generate_password
from app.internal.users.presentation.entities import UserIn


def start(update: Update, context: CallbackContext):
    tg_id = update.effective_chat.id
    password = generate_password()
    user_in = UserIn(
        telegram_username=update.effective_chat.username,
        first_name=update.effective_chat.first_name,
        last_name=update.effective_chat.last_name,
        phone_number="",
        password=password,
    )
    try:
        user_repo.add_user(user_in)
    except IntegrityError:
        context.bot.send_message(chat_id=tg_id, text=StartHints.ALREADY_USER.value)
    else:
        context.bot.send_message(chat_id=tg_id, text=StartHints.SUCCESS.value)
        context.bot.send_message(chat_id=tg_id, text=f"{StartHints.PASSWORD.value} {password}")


start_command_handler = CommandHandler("start", start)
