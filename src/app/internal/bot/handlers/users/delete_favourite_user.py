from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler, Filters, MessageHandler

from app.internal.bot.handlers import user_repo

from ..constants import Common, RemoveUserFromFavourites
from ..utils import cancel

ENTER_USERNAME = 0


def delete_favourite_user(update: Update, context: CallbackContext):
    user = user_repo.get_user_by_username(update.effective_chat.username)
    chat_id = update.effective_chat.id
    if not user:
        context.bot.send_message(chat_id=chat_id, text=Common.NO_USER.value)
        return ConversationHandler.END

    context.user_data["user"] = user
    context.bot.send_message(chat_id=chat_id, text=Common.ENTER_USERNAME.value)
    return ENTER_USERNAME


def final(update: Update, context: CallbackContext):
    chat_id = update.effective_chat.id
    username_to_delete = update.message.text.replace("@", "")
    user_to_delete = user_repo.get_user_by_username(username_to_delete)
    favourite_users_usernames = user_repo.get_favourite_users(context.user_data["user"])
    if username_to_delete not in favourite_users_usernames:
        context.bot.send_message(chat_id=chat_id, text=RemoveUserFromFavourites.NOT_IN_FAVS.value)
        return ConversationHandler.END
    user_repo.delete_favourite_user(context.user_data["user"], user_to_delete)
    context.bot.send_message(chat_id=chat_id, text=Common.SUCCESS.value)
    return ConversationHandler.END


delete_favourite_user_conv_handler = ConversationHandler(
    entry_points=[CommandHandler("delete_favourite_user", delete_favourite_user)],
    states={
        ENTER_USERNAME: [MessageHandler(Filters.text, final)],
    },
    fallbacks=[CommandHandler("cancel", cancel)],
)
