from django.conf import settings
from telegram import Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler, Filters, MessageHandler

from app.internal.bot.handlers import user_repo
from app.internal.bot.handlers.constants import Common, SetPhoneHints

from ..utils import cancel

ENTER_PHONE = 0


def set_phone(update: Update, context: CallbackContext):
    user = user_repo.get_user_by_username(update.effective_chat.username)
    tg_id = update.effective_chat.id
    if not user:
        context.bot.send_message(chat_id=tg_id, text=Common.NO_USER.value)
        return ConversationHandler.END
    context.user_data["user"] = user
    context.bot.send_message(chat_id=tg_id, text=SetPhoneHints.ENTER_PHONE_NUMBER.value)
    return ENTER_PHONE


def final(update: Update, context: CallbackContext):
    phone_number = update.message.text
    user_repo.set_phone(context.user_data["user"], phone_number)
    context.bot.send_message(chat_id=update.effective_chat.id, text=SetPhoneHints.SUCCESS.value)
    return ConversationHandler.END


set_phone_conv_handler = ConversationHandler(
    entry_points=[CommandHandler("set_phone", set_phone)],
    states={
        ENTER_PHONE: [MessageHandler(Filters.regex(settings.PHONE_NUMBER_REGEX), final)],
    },
    fallbacks=[CommandHandler("cancel", cancel)],
)
