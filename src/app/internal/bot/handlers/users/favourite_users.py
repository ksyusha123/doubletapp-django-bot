from telegram import Update
from telegram.ext import CallbackContext, CommandHandler

from app.internal.bot.handlers import user_repo
from app.internal.bot.handlers.constants import Common, ShowFavouriteUsers


def favourite_users(update: Update, context: CallbackContext):
    user = user_repo.get_user_by_username(update.effective_chat.username)
    chat_id = update.effective_chat.id
    if not user:
        context.bot.send_message(chat_id=chat_id, text=Common.NO_USER.value)
        return

    favourite_users_usernames = user_repo.get_favourite_users(user)
    if not favourite_users_usernames:
        context.bot.send_message(chat_id=chat_id, text=ShowFavouriteUsers.NO_FAV_USERS.value)
    context.bot.send_message(chat_id=chat_id, text="\n".join(favourite_users_usernames))


favourite_users_command_handler = CommandHandler("favourite_users", favourite_users)
