from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler, Filters, MessageHandler

from app.internal.bot.handlers import card_repo, transactions_repo, user_repo

from ..constants import Common, MakeAccountStatement
from ..utils import cancel, choose_account, choose_card, convert_date_str_to_datetime, pretty_print_statement

CHOOSE_ITEM_NUMBER, PROCESS_DESTINATION, ENTER_END_DATE, FINAL = range(4)


def make_account_statement_start(update: Update, context: CallbackContext):
    user = user_repo.get_user_by_username(update.effective_chat.username)
    tg_id = update.effective_chat.id
    if not user:
        context.bot.send_message(chat_id=tg_id, text=Common.NO_USER.value)
        return ConversationHandler.END
    keyboard = [["Account", "Card"]]
    reply_markup = ReplyKeyboardMarkup(keyboard, one_time_keyboard=True, resize_keyboard=True)
    update.message.reply_text(MakeAccountStatement.BY.value, reply_markup=reply_markup)
    return CHOOSE_ITEM_NUMBER


def choose_item_number(update: Update, context: CallbackContext):
    item_type = update.message.text
    if item_type == "Account":
        return choose_account(update, context, Common.NO_ACCOUNTS.value, PROCESS_DESTINATION)
    return choose_card(update, context, Common.NO_CARDS.value, PROCESS_DESTINATION)


def process_card_destination(update: Update, context: CallbackContext):
    card_number = update.message.text
    account_number = card_repo.get_account_by_card_number(card_number)
    context.user_data["account"] = account_number
    return enter_start_date(update, context)


def process_account_destination(update: Update, context: CallbackContext):
    account_number = update.message.text
    context.user_data["account"] = account_number
    return enter_start_date(update, context)


def enter_start_date(update: Update, _):
    update.message.reply_text(MakeAccountStatement.ENTER_START_DATE.value, reply_markup=ReplyKeyboardRemove())
    return ENTER_END_DATE


def enter_end_date(update: Update, context: CallbackContext):
    start_date = update.message.text
    try:
        date = convert_date_str_to_datetime(start_date)
    except ValueError:
        context.bot.send_message(chat_id=update.effective_chat.id, text=MakeAccountStatement.WRONG_DATE.value)
        return ConversationHandler.END
    context.user_data["start_date"] = date
    update.message.reply_text(MakeAccountStatement.ENTER_END_DATE.value)
    return FINAL


def process_dates(update: Update, context: CallbackContext):
    end_date_str = update.message.text
    try:
        end_date = convert_date_str_to_datetime(end_date_str)
    except ValueError:
        context.bot.send_message(chat_id=update.effective_chat.id, text=MakeAccountStatement.WRONG_DATE.value)
        return ConversationHandler.END
    start_date = context.user_data["start_date"]
    if end_date < start_date:
        context.bot.send_message(chat_id=update.effective_chat.id, text=MakeAccountStatement.END_HAS_TO_BE_LATER.value)
        return ConversationHandler.END
    statement = transactions_repo.get_transactions_for_account(context.user_data["account"], start_date, end_date)
    pretty_statement = pretty_print_statement(statement)
    context.bot.send_message(chat_id=update.effective_chat.id, text=pretty_statement)
    return ConversationHandler.END


make_statement_conv_handler = ConversationHandler(
    entry_points=[CommandHandler("make_statement", make_account_statement_start)],
    states={
        CHOOSE_ITEM_NUMBER: [MessageHandler(Filters.regex(r"^Account|Card$"), choose_item_number)],
        PROCESS_DESTINATION: [
            MessageHandler(Filters.regex(r"^\d{20}$"), process_account_destination),
            MessageHandler(Filters.regex(r"^\d{16}$"), process_card_destination),
        ],
        ENTER_END_DATE: [
            MessageHandler(Filters.regex(r"^[0-9]{2}.[0-9]{2}.[0-9]{4}$"), enter_end_date),
        ],
        FINAL: [
            MessageHandler(Filters.regex(r"^[0-9]{2}.[0-9]{2}.[0-9]{4}$"), process_dates),
        ],
    },
    fallbacks=[CommandHandler("cancel", cancel)],
)
