from telegram import Update
from telegram.ext import CallbackContext, CommandHandler

from app.internal.bot.handlers import accounts_repo, transactions_repo, user_repo

from ..constants import Common, NewTransactions
from ..utils import pretty_print_single_transaction


def new_transactions(update: Update, context: CallbackContext):
    user = user_repo.get_user_by_username(update.effective_chat.username)
    tg_id = update.effective_chat.id
    if not user:
        context.bot.send_message(chat_id=tg_id, text=Common.NO_USER.value)
        return
    user_accounts = accounts_repo.get_user_accounts_number(user)
    transactions = transactions_repo.get_new_transactions(user_accounts)
    if not transactions:
        context.bot.send_message(chat_id=tg_id, text=NewTransactions.NO_NEW_TRANSACTIONS.value)
        return
    for transaction in transactions:
        transactions_repo.mark_transaction_viewed(transaction)
        pretty_statement = pretty_print_single_transaction(
            transaction.target_account.owner.telegram_username, transaction.money_amount, transaction.account_balance
        )
        context.bot.send_message(chat_id=tg_id, text=pretty_statement)
        postcard = transaction.postcard
        if postcard:
            image = postcard.image
            context.bot.send_photo(chat_id=tg_id, photo=image)


new_transactions_command_handler = CommandHandler("new_transactions", new_transactions)
