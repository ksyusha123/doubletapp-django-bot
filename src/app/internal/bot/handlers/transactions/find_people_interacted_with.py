from telegram import Update
from telegram.ext import CallbackContext, CommandHandler

from app.internal.accounts.db.repositories import AccountsRepository
from app.internal.transactions.db.repositories import TransactionsRepository
from app.internal.users.db.repositories import UserRepository

from ..constants import Common, FindPeopleInteractedWith

user_repo = UserRepository()
accounts_repo = AccountsRepository()
logs_repo = TransactionsRepository()


def find_people_interacted_with(update: Update, context: CallbackContext):
    user = user_repo.get_user_by_username(update.effective_chat.username)
    if not user:
        context.bot.send_message(chat_id=update.effective_chat.id, text=Common.NO_USER.value)
        return
    accounts_numbers = accounts_repo.get_user_accounts_number(user)
    people_interacted_with = logs_repo.find_people_interacted_with(accounts_numbers)
    if not people_interacted_with:
        context.bot.send_message(chat_id=update.effective_chat.id, text=FindPeopleInteractedWith.NO_PEOPLE.value)
        return
    context.bot.send_message(chat_id=update.effective_chat.id, text="\n".join(people_interacted_with))


find_people_interacted_with_command_handler = CommandHandler("people_interacted_with", find_people_interacted_with)
