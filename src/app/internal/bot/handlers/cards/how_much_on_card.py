from telegram import ReplyKeyboardRemove, Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler, Filters, MessageHandler

from app.internal.accounts.db.repositories import AccountsRepository
from app.internal.bot.handlers.constants import HowMuchOnCardHints
from app.internal.cards.db.repositories import CardRepository
from app.internal.users.db.repositories import UserRepository

from ..utils import cancel, choose_items

GET_CARD_AMOUNT = 0

user_repo = UserRepository()
cards_repo = CardRepository()
accounts_repo = AccountsRepository()


def how_much_on_card(update: Update, context: CallbackContext):
    user = user_repo.get_user_by_username(update.effective_chat.username)
    if not user:
        context.bot.send_message(chat_id=update.effective_chat.id, text=HowMuchOnCardHints.NO_USER.value)
        return ConversationHandler.END

    card_numbers = cards_repo.get_user_cards_numbers(user)

    return choose_items(
        card_numbers,
        update,
        context,
        HowMuchOnCardHints.NO_CARDS.value,
        GET_CARD_AMOUNT,
    )


def get_amount_for_card(update: Update, _):
    card_number = update.message.text
    account = cards_repo.get_account_by_card_number(card_number)
    update.message.reply_text(
        str(account.amount),
        reply_markup=ReplyKeyboardRemove(),
    )
    return ConversationHandler.END


how_much_on_card_conv_handler = ConversationHandler(
    entry_points=[CommandHandler("how_much_on_card", how_much_on_card)],
    states={GET_CARD_AMOUNT: [MessageHandler(Filters.regex(r"^\d{16}$"), get_amount_for_card)]},
    fallbacks=[CommandHandler("cancel", cancel)],
)
