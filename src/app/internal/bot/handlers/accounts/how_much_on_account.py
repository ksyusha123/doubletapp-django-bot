from telegram import ReplyKeyboardRemove, Update
from telegram.ext import CallbackContext, CommandHandler, ConversationHandler, Filters, MessageHandler

from app.internal.accounts.db.repositories import AccountsRepository
from app.internal.bot.handlers.constants import HowMuchOnAccountHints
from app.internal.users.db.repositories import UserRepository

from ..utils import cancel, choose_items

user_repo = UserRepository()
accounts_repo = AccountsRepository()

GET_ACCOUNT_AMOUNT = 0


def how_much_on_acc(update: Update, context: CallbackContext):
    user = user_repo.get_user_by_username(update.effective_chat.username)
    if not user:
        context.bot.send_message(chat_id=update.effective_chat.id, text=HowMuchOnAccountHints.NO_USER.value)
        return ConversationHandler.END

    accounts_numbers = accounts_repo.get_user_accounts_number(user)

    return choose_items(
        accounts_numbers,
        update,
        context,
        HowMuchOnAccountHints.NO_ACCOUNTS.value,
        GET_ACCOUNT_AMOUNT,
    )


def get_amount_for_account(update: Update, _):
    account_number = update.message.text
    amount = accounts_repo.get_amount_for_account_by_number(account_number)
    update.message.reply_text(
        str(amount),
        reply_markup=ReplyKeyboardRemove(),
    )
    return ConversationHandler.END


how_much_on_acc_conv_handler = ConversationHandler(
    entry_points=[CommandHandler("how_much_on_account", how_much_on_acc)],
    states={GET_ACCOUNT_AMOUNT: [MessageHandler(Filters.regex(r"^\d{20}$"), get_amount_for_account)]},
    fallbacks=[CommandHandler("cancel", cancel)],
)
