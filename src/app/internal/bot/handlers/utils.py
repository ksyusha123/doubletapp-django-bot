from datetime import datetime
from typing import List

from django.db.models.query import QuerySet
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove, Update
from telegram.ext import CallbackContext, ConversationHandler

from app.internal.bot.handlers import accounts_repo, card_repo, postcard_repo, user_repo
from app.internal.bot.handlers.constants import Common


def convert_date_str_to_datetime(date_str: str) -> datetime.date:
    date_format = "%d.%m.%Y"
    return datetime.strptime(date_str, date_format).date()


def convert_datetime_to_str(date_time: datetime) -> str:
    date_format = "%H:%M"
    return date_time.strftime(date_format)


def cancel(update, context):
    update.message.reply_text("Wrong arguments", reply_markup=ReplyKeyboardRemove())
    return ConversationHandler.END


def choose_items(
    items: QuerySet[str], update: Update, context: CallbackContext, no_items_msg: str, next_stage: int
) -> int:
    if not items:
        context.bot.send_message(chat_id=update.effective_chat.id, text=no_items_msg)
        return ConversationHandler.END

    reply_markup = make_keyboard(items)
    update.message.reply_text(Common.CHOOSE_ITEM.value, reply_markup=reply_markup)
    return next_stage


def choose_account(update: Update, context: CallbackContext, no_items_msg: str, next_stage: int) -> int:
    user = user_repo.get_user_by_username(update.effective_chat.username)
    if not user:
        context.bot.send_message(chat_id=update.effective_chat.id, text=Common.NO_USER.value)
        return ConversationHandler.END

    accounts_numbers = accounts_repo.get_user_accounts_number(user)

    return choose_items(accounts_numbers, update, context, no_items_msg, next_stage)


def choose_card(update: Update, context: CallbackContext, no_items_msg: str, next_stage: int) -> int:
    user = user_repo.get_user_by_username(update.effective_chat.username)
    if not user:
        context.bot.send_message(chat_id=update.effective_chat.id, text=Common.NO_USER.value)
        return ConversationHandler.END

    accounts_numbers = card_repo.get_user_cards_numbers(user)

    return choose_items(accounts_numbers, update, context, no_items_msg, next_stage)


def pretty_print_statement(statement: QuerySet) -> str:
    pretty_statement = []
    for record in statement:
        pretty_record = pretty_print_single_transaction(
            record["target_account__owner__telegram_username"], record["money_amount"], record["account_balance"]
        )
        postcard_id = record["postcard"]
        if postcard_id:
            presigned_url = postcard_repo.get_postcard_by_id(postcard_id).image.url
            pretty_record += f"postcard: {presigned_url}\n"

        pretty_statement.append(pretty_record)
    return "\n".join(pretty_statement)


def pretty_print_single_transaction(username: str, money_amount: str, account_balance: str) -> str:
    return f"telegram username: {username}\n" f"money amount: {money_amount}\n" f"account balance {account_balance}\n"


def make_keyboard(items: List) -> ReplyKeyboardMarkup:
    keyboard = [list(items)]
    return ReplyKeyboardMarkup(keyboard, one_time_keyboard=True, resize_keyboard=True)
