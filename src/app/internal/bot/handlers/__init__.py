from app.internal.accounts.db.repositories import AccountsRepository
from app.internal.cards.db.repositories import CardRepository
from app.internal.postcards.db.repositories import PostcardRepository
from app.internal.transactions.db.repositories import TransactionsRepository
from app.internal.users.db.repositories import UserRepository

user_repo = UserRepository()
accounts_repo = AccountsRepository()
card_repo = CardRepository()
transactions_repo = TransactionsRepository()
postcard_repo = PostcardRepository()
