import json

from django.http import JsonResponse
from django.views import View

from app.internal.bot.bot import bot


class BotView(View):
    def post(self, request, *args, **kwargs):
        update_json = json.loads(request.body)
        bot.process_update(update_json)
        return JsonResponse({"OK": "update processed"})

    def get(self, request, *args, **kwargs):
        return JsonResponse({"OK": "update not processed"})
