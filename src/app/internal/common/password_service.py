import hashlib
import secrets
import string
from typing import Tuple

from django.conf import settings

password_length = 8


def generate_password() -> str:
    alphabet = string.ascii_letters + string.digits
    password = "".join(secrets.choice(alphabet) for _ in range(password_length))
    return password


def hash_password(password: str) -> str:
    return hashlib.sha1((settings.SALT + password).encode()).hexdigest()


def generate_and_hash_password() -> Tuple[str, str]:
    password = generate_password()
    return password, hash_password(password)
