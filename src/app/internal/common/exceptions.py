class NoPhoneNumberException(Exception):
    def __init__(self):
        super().__init__()
        self.message = "phone number must be set"


class NotFoundException(Exception):
    def __init__(self, id, name):
        super().__init__()
        self.name = name
        self.id = id


class BadRequestException(Exception):
    def __init__(self, message):
        super().__init__()
        self.message = message


class AlreadyUserException(Exception):
    def __init__(self, telegram_username):
        super().__init__()
        self.username = telegram_username


class PermissionDenied(Exception):
    def __init__(self, id, name):
        super().__init__()
        self.message = f"permission denied for {name} {id}"
