from datetime import datetime

import jwt
from django.conf import settings
from django.http import HttpRequest
from ninja.security import HttpBearer

from app.internal.users.db.repositories import UserRepository

user_repo = UserRepository()


class HTTPJWTAuth(HttpBearer):
    def authenticate(self, request: HttpRequest, token):
        try:
            payload = jwt.decode(token, settings.JWT_SECRET, algorithms=["HS256"])
            if payload["exp"] < datetime.now().timestamp():
                return None
            user = user_repo.get_user_by_username(payload["username"])
            request.user = user
        except:
            return None

        return token
