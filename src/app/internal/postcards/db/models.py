from pathlib import Path
from uuid import uuid4

from django.db import models


def rename_and_upload(instance, _):
    upload_to = "postcards"
    filename = Path(f"{instance.pk}.png")
    return upload_to / filename


class Postcard(models.Model):
    id = models.UUIDField(default=uuid4, editable=False, primary_key=True)
    image = models.FileField(upload_to=rename_and_upload)
