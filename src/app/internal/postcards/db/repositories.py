import logging

from django.core.files.base import ContentFile

from ..db.models import Postcard

logger = logging.getLogger(__name__)


class PostcardRepository:
    def add_postcard(self, content: bytes) -> Postcard:
        image = ContentFile(content, "postcard")
        try:
            postcard = Postcard.objects.create(image=image)
            logger.info(f"Download image {postcard.image.name}")
            return postcard
        except:
            logger.error(f"Failed to download content: {content}")
            raise

    def get_postcard_by_id(self, id: str) -> Postcard:
        return Postcard.objects.get(pk=id)
