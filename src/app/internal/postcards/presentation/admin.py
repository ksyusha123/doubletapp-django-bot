from django.contrib import admin

from ..db.models import Postcard


@admin.register(Postcard)
class SimpleUserAdmin(admin.ModelAdmin):
    pass
