from ninja import NinjaAPI

from .db.repositories import CardRepository
from .presentation.handlers import CardHandlers
from .presentation.routers import add_card_router


def add_cards_api(api: NinjaAPI):
    card_repo = CardRepository()
    card_handlers = CardHandlers(card_repo=card_repo)
    add_card_router(api, card_handlers)
