from ninja import Body

from ..db.repositories import CardRepository
from .entities import CardOut, CardSchema


class CardHandlers:
    def __init__(self, card_repo: CardRepository):
        self._card_repo = card_repo

    def reissue(self, request, card_number: str, card_data: CardSchema = Body(...)) -> CardSchema:
        card = self._card_repo.update_or_create_card(card_number, card_data.dict())
        return CardOut.from_orm(card)
