from ninja.orm import create_schema

from ..db.models import Card

CardSchema = create_schema(Card, exclude=["number"])


class CardOut(CardSchema):
    number: str
