from ninja import NinjaAPI, Router

from app.internal.common.responses import ErrorResponse

from .entities import CardOut
from .handlers import CardHandlers


def add_card_router(api: NinjaAPI, card_handlers: CardHandlers):
    cards_router = get_cards_router(card_handlers)
    api.add_router("/cards", cards_router)


def get_cards_router(cards_handlers: CardHandlers):
    router = Router(tags=["cards"])

    router.add_api_operation(
        "/{int:card_number}",
        ["PUT"],
        cards_handlers.reissue,
        response={200: CardOut, 400: ErrorResponse},
    )

    return router
