import logging
import re

from django.core.exceptions import ValidationError
from django.db.models.query import QuerySet

from app.internal.accounts.db.models import Account
from app.internal.accounts.db.repositories import AccountsRepository
from app.internal.cards.db.models import Card
from app.internal.common.exceptions import NotFoundException
from app.internal.users.db.models import User

logger = logging.getLogger(__name__)
accounts_repo = AccountsRepository()


class CardRepository:
    def get_user_cards_numbers(self, user: User) -> QuerySet[str]:
        account_numbers = accounts_repo.get_user_accounts_number(user)
        card_numbers = Card.objects.filter(account_id__in=account_numbers).values_list("number", flat=True)
        return card_numbers

    def get_account_by_card_number(self, card_number: str) -> Account | None:
        return Card.objects.filter(pk=card_number).first().account

    def get_card_by_number(self, card_number: str) -> Card | None:
        return Card.objects.filter(pk=card_number).first()

    def update_or_create_card(self, card_number: str, card_data: dict) -> Card:
        if not re.match(r"^\d{16}$", card_number):
            raise ValidationError("card number must consist of 16 digits")
        account_number = card_data["account"]
        account = accounts_repo.get_account_by_number(account_number)
        if not account:
            raise NotFoundException(account_number, "account")
        card_data["account"] = account
        card, created = Card.objects.update_or_create(defaults=card_data, number=card_number)
        log_msg = f"Card {card_number} was "
        log_msg += "created" if created else "updated"
        return card
