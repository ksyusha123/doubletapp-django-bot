import re

from django.core.exceptions import ValidationError
from django.db import models

from app.internal.accounts.db.models import Account


def validate_card_number(card_number: str):
    if not re.match(r"^\d{16}$", card_number):
        raise ValidationError("Incorrect card number")


class Card(models.Model):
    number = models.CharField(primary_key=True, max_length=16, validators=[validate_card_number])
    account = models.ForeignKey(Account, on_delete=models.CASCADE)

    def __str__(self):
        return f"account: {self.account.owner.telegram_username}\nnumber: {self.number}"
