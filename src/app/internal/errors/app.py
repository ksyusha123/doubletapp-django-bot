from functools import partial

from django.core.exceptions import ValidationError
from ninja import NinjaAPI

from ..common.exceptions import AlreadyUserException, BadRequestException, NotFoundException, PermissionDenied
from .handlers import (
    already_user_exception_handler,
    bad_request_exception_handler,
    object_not_found_exception_handler,
    permission_denied_exception_handler,
)


def set_exception_handlers(api: NinjaAPI):
    api.add_exception_handler(NotFoundException, partial(object_not_found_exception_handler, api=api))
    api.add_exception_handler(AlreadyUserException, partial(already_user_exception_handler, api=api))
    api.add_exception_handler(BadRequestException, partial(bad_request_exception_handler, api=api))
    api.add_exception_handler(ValidationError, partial(bad_request_exception_handler, api=api))
    api.add_exception_handler(PermissionDenied, partial(permission_denied_exception_handler, api=api))
