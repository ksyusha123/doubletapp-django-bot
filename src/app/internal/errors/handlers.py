from ninja import NinjaAPI


def object_not_found_exception_handler(request, exc, api: NinjaAPI):
    return api.create_response(
        request,
        {"message": f"{exc.name} with id {exc.id} not found"},
        status=404,
    )


def already_user_exception_handler(request, exc, api: NinjaAPI):
    return api.create_response(
        request,
        {"message": f"already have user {exc.username}"},
        status=400,
    )


def bad_request_exception_handler(request, exc, api: NinjaAPI):
    return api.create_response(
        request,
        {"message": exc.message},
        status=400,
    )


def permission_denied_exception_handler(request, exc, api: NinjaAPI):
    return api.create_response(
        request,
        {"message": exc.message},
        status=403,
    )
