from prometheus_client import Gauge

from .db.repositories import TransactionsRepository


def config_transactions_metrics(transactions_repo: TransactionsRepository):
    transferred_money_amount = Gauge("transferred_money_amount", "transferred money amount")
    transferred_money_amount.set_function(transactions_repo.get_transferred_money)
