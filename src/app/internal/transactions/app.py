from ninja import NinjaAPI

from .db.repositories import TransactionsRepository
from .metrics import config_transactions_metrics
from .presentation.handlers import TransactionHandlers
from .presentation.routers import add_transactions_router


def add_log_records_api(api: NinjaAPI):
    transaction_repo = TransactionsRepository()
    transactions_handler = TransactionHandlers(transaction_repo=transaction_repo)
    config_transactions_metrics(transaction_repo)
    add_transactions_router(api, transactions_handler)
