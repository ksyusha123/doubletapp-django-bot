from datetime import datetime, timedelta
from decimal import Decimal

from django.db.models import QuerySet

from app.internal.accounts.db.models import Account
from app.internal.postcards.db.models import Postcard

from .models import Transaction


class TransactionsRepository:
    def create_transaction(
        self, account: Account, target_account: Account, money_amount: Decimal, balance: Decimal
    ) -> Transaction:
        return Transaction.objects.create(
            account=account,
            target_account=target_account,
            money_amount=money_amount,
            account_balance=balance,
        )

    def get_transactions_for_account(
        self, account_number: str, start_date: datetime.date, end_date: datetime.date
    ) -> QuerySet[str]:
        end_date += timedelta(days=1)
        return Transaction.objects.filter(account_id=account_number, date__gte=start_date, date__lt=end_date).values(
            "money_amount", "target_account__owner__telegram_username", "account_balance", "postcard"
        )

    def find_people_interacted_with(self, accounts_numbers: QuerySet[str]) -> QuerySet[str]:
        return (
            Transaction.objects.filter(account_id__in=accounts_numbers)
            .values_list("target_account__owner__telegram_username", flat=True)
            .distinct()
        )

    def get_transaction_by_id(self, id: str) -> Transaction:
        return Transaction.objects.get(pk=id)

    def attach_postcard(self, transaction: Transaction, postcard: Postcard) -> None:
        transaction.postcard = postcard
        transaction.save(update_fields=("postcard",))

    def get_new_transactions(self, accounts_numbers: QuerySet[str]) -> QuerySet[Transaction]:
        return Transaction.objects.filter(account_id__in=accounts_numbers, viewed=False)

    def mark_transaction_viewed(self, transaction: Transaction) -> None:
        transaction.viewed = True
        transaction.save(update_fields=("viewed",))

    def get_transferred_money(self) -> float:
        return float(sum(Transaction.objects.filter(money_amount__gte=0).values_list("money_amount", flat=True)))
