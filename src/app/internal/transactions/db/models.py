from uuid import uuid4

from django.db import models

from app.internal.accounts.db.models import Account
from app.internal.postcards.db.models import Postcard


class Transaction(models.Model):
    id = models.UUIDField(default=uuid4, editable=False, primary_key=True)
    date = models.DateTimeField(auto_now_add=True)
    account = models.ForeignKey(Account, on_delete=models.PROTECT, related_name="account_from")
    money_amount = models.DecimalField(max_digits=10, decimal_places=2)
    target_account = models.ForeignKey(Account, on_delete=models.PROTECT, related_name="account_to")
    account_balance = models.DecimalField(max_digits=10, decimal_places=2)
    postcard = models.ForeignKey(Postcard, on_delete=models.SET_NULL, null=True)
    viewed = models.BooleanField(default=False)

    def __str__(self):
        return (
            f"date: {self.date} "
            f"account: {self.account} "
            f"money amount: {self.money_amount} "
            f"target account: {self.target_account} "
            f"account balance: {self.account_balance}"
        )
