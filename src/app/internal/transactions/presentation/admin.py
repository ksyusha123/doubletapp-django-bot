from django.contrib import admin

from ..db.models import Transaction


@admin.register(Transaction)
class SimpleUserAdmin(admin.ModelAdmin):
    pass
