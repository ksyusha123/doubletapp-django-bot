from ..db.repositories import TransactionsRepository


class TransactionHandlers:
    def __init__(self, transaction_repo: TransactionsRepository):
        self._transaction_repo = transaction_repo
