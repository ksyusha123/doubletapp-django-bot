from ninja import NinjaAPI, Router

from .handlers import TransactionHandlers


def add_transactions_router(api: NinjaAPI, transactions_handlers: TransactionHandlers):
    transactions_router = get_transactions_router(transactions_handlers)
    api.add_router("/transactions", transactions_router)


def get_transactions_router(transactions_handlers: TransactionHandlers):
    router = Router(tags=["transactions"])

    return router
