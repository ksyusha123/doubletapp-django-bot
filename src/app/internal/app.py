from ninja import NinjaAPI

from .accounts.app import add_accounts_api
from .authentication.app import add_auth_api
from .cards.app import add_cards_api
from .errors.app import set_exception_handlers
from .middlewares.http_jwt_auth import HTTPJWTAuth
from .transactions.app import add_log_records_api
from .users.app import add_users_api


def get_api() -> NinjaAPI:
    api = NinjaAPI(
        title="backend-course",
        version="1.0.0",
        auth=[HTTPJWTAuth()],
    )

    add_users_api(api)
    add_accounts_api(api)
    add_cards_api(api)
    add_log_records_api(api)
    add_auth_api(api)

    set_exception_handlers(api)

    return api
