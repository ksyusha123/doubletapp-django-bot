from ninja import NinjaAPI

from ..users.db.repositories import UserRepository
from .db.repositories import IssuedTokenRepository
from .presentation.handlers import AuthenticationHandlers
from .presentation.routers import add_authentication_router


def add_auth_api(api: NinjaAPI):
    user_repo = UserRepository()
    issued_token_repo = IssuedTokenRepository()
    auth_handler = AuthenticationHandlers(user_repo=user_repo, issued_token_repo=issued_token_repo)
    add_authentication_router(api, auth_handler)
