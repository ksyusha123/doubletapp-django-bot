from ninja import Body

from app.internal.common.exceptions import BadRequestException, NotFoundException
from app.internal.users.db.repositories import UserRepository

from ..db.repositories import IssuedTokenRepository
from .entities import CredentialsSchema, JWTTokensSchema


class AuthenticationHandlers:
    def __init__(self, issued_token_repo: IssuedTokenRepository, user_repo: UserRepository):
        self._issued_token_repo = issued_token_repo
        self._user_repo = user_repo

    def login(self, request, credentials: CredentialsSchema = Body(...)) -> JWTTokensSchema:
        user = self._user_repo.get_user_by_username(credentials.username)
        if not user:
            raise NotFoundException(credentials.username, "user")
        self._user_repo.validate_by_username(user, credentials.password)
        self._issued_token_repo.revoke_all_tokens(user)
        access_token, refresh_token = self._issued_token_repo.generate_access_refresh_tokens(user)
        return JWTTokensSchema(access_token=access_token, refresh_token=refresh_token)

    def refresh(self, request, refresh_token) -> JWTTokensSchema:
        issued_token = self._issued_token_repo.get_token_model_by_raw_token(refresh_token)
        if not issued_token:
            raise NotFoundException(refresh_token, "refresh token")
        has_expired = self._issued_token_repo.check_if_token_has_expired(issued_token)
        if has_expired:
            raise BadRequestException("refresh token has expired")
        if issued_token.revoked:
            self._issued_token_repo.revoke_all_tokens(issued_token.user)
            raise BadRequestException("refresh token was revoked")
        self._issued_token_repo.revoke_token(issued_token)
        new_access_token, new_refresh_token = self._issued_token_repo.generate_access_refresh_tokens(issued_token.user)
        return JWTTokensSchema(access_token=new_access_token, refresh_token=new_refresh_token)
