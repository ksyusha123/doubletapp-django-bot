from ninja import NinjaAPI, Router

from app.internal.common.responses import ErrorResponse

from .entities import JWTTokensSchema
from .handlers import AuthenticationHandlers


def add_authentication_router(api: NinjaAPI, issued_token_handlers: AuthenticationHandlers):
    issued_token_router = get_authentication_router(issued_token_handlers)
    api.add_router("", issued_token_router)


def get_authentication_router(authentication_handlers: AuthenticationHandlers):
    router = Router(tags=["authentication"])

    router.add_api_operation(
        "/login",
        ["POST"],
        authentication_handlers.login,
        auth=None,
        response={200: JWTTokensSchema, 400: ErrorResponse},
    )

    router.add_api_operation(
        "/refresh",
        ["POST"],
        authentication_handlers.refresh,
        auth=None,
        response={200: JWTTokensSchema, 400: ErrorResponse},
    )

    return router
