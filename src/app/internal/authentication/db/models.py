from django.db import models

from app.internal.users.db.models import User


class IssuedToken(models.Model):
    jti = models.CharField(primary_key=True, max_length=255)
    user = models.ForeignKey(User, related_name="refresh_tokens", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    revoked = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.user.telegram_username} " f"{'revoked' if self.revoked else ''}"
