from datetime import datetime
from typing import Tuple

import jwt
from django.conf import settings

from app.internal.users.db.models import User

from .models import IssuedToken


def generate_jwt_token(username: str, expires_timedelta: int) -> bytes:
    return jwt.encode(
        {"username": username, "exp": datetime.now().timestamp() + expires_timedelta},
        settings.JWT_SECRET,
        algorithm="HS256",
    )


class IssuedTokenRepository:
    def revoke_all_tokens(self, user: User):
        user.refresh_tokens.filter(revoked=False).update(revoked=True)

    def revoke_token(self, token: IssuedToken):
        token.revoked = True
        token.save()

    def generate_access_refresh_tokens(self, user: User) -> Tuple[bytes, bytes]:
        access_token = generate_jwt_token(user.telegram_username, settings.ACCESS_TOKEN_EXPIRES_PERIOD)
        refresh_token = generate_jwt_token(user.telegram_username, settings.REFRESH_TOKEN_EXPIRES_PERIOD)
        IssuedToken.objects.create(jti=refresh_token, user=user)
        return access_token, refresh_token

    def get_token_model_by_raw_token(self, raw_token: str) -> IssuedToken:
        return IssuedToken.objects.filter(jti=raw_token).first()

    def check_if_token_has_expired(self, token: IssuedToken) -> bool:
        return token.created_at.timestamp() + settings.REFRESH_TOKEN_EXPIRES_PERIOD < datetime.now().timestamp()
