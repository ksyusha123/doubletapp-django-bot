from django.contrib import admin

from app.internal.accounts.presentation.admin import Account
from app.internal.admin.presentation.admin import AdminUser
from app.internal.authentication.presentation.admin import IssuedToken
from app.internal.cards.presentation.admin import Card
from app.internal.postcards.presentation.admin import Postcard
from app.internal.transactions.presentation.admin import Transaction
from app.internal.users.presentation.admin import User

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
