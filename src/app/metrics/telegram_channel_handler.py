from logging import Handler, LogRecord

import requests
from django.conf import settings


def make_telegram_message_url(tg_from: str) -> str:
    return f"https://api.telegram.org/{tg_from}/sendMessage"


class TelegramChannelHandler(Handler):
    def emit(self, record: LogRecord) -> None:
        log_entry = self.format(record)
        url = make_telegram_message_url(f"bot{settings.BOT_TOKEN}")
        requests.post(
            url,
            data={
                "chat_id": settings.LOG_CHANNEL_ID,
                "text": log_entry,
            },
        )
