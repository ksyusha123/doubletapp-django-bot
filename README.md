# Backend course  

App: https://kast0.backend22.2tapp.cc  
Bot: ```@django_2tapp_bot```  


## Api
https://kast0.backend22.2tapp.cc/api/docs  

## Bot commands  
- /start     
- /set_phone  
- /me     
- /how_much_on_account  
- /how_much_on_card  
- /favourite_users  
- /add_favourite_user  
- /delete_favourite_user  
- /transfer_money  
- /make_statement  
- /new_transactions  


## Perfomance testing results  
https://overload.yandex.net/518071#tab=test_data&tags=&plot_groups=main&machines=&metrics=&slider_start=1650476324&slider_end=1650476384
